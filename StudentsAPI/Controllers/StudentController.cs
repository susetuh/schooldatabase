﻿using Microsoft.AspNetCore.Mvc;
using StudentsRepository;
using StudentsRepository.Interfaces;
using StudentsAPI.DTO;
using StudentsDB.Entities;
using System.Collections.Generic;

namespace StudentsAPI.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class StudentController : Controller
    {
        private readonly IStudentRepository repository;
        public StudentController()
        {
            repository = new StudentRepository();
        }

        [HttpGet]
        public ActionResult<List<Student>> Students()
        {
            return Ok(repository.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<Student> Student([FromRoute] int id)
        {
            var student = repository.Get(id);
            if (student == null)
                return NotFound();
            return Ok(student);
        }

        [HttpPost]
        public ActionResult<Student> Create([FromBody] Student student)
        {
            repository.Add(student);
            return CreatedAtAction(nameof(Create), student);
        }

        [HttpPut("{id}")]
        public ActionResult Modify([FromRoute] int id, [FromBody] Student student)
        {
            repository.Update(id, student);
            return NoContent();
        }

        [HttpPatch("{id}")]
        public ActionResult ChangeName([FromRoute] int id, [FromBody] IdName dto)
        {
            repository.ChangeName(id, dto.Name);
            return NoContent();
        }


        [HttpDelete("{id}")]
        public ActionResult Delete([FromRoute] int id)
        {
            repository.Delete(id);
            return NoContent();
        }
    }
}
