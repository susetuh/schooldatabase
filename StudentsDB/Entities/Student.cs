﻿namespace StudentsDB.Entities
{
    public class Student
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public int Age { get; set; }
        public string School { get; set; }

    }
}
