﻿using System.Collections.Generic;

namespace StudentsRepository.Interfaces
{
    public interface IRepository<T>
    {
        T Get(int id);
        List<T> GetAll();
        void Add(T student);
        void Update(int id, T student);
        void Delete(int id);
    }
}
