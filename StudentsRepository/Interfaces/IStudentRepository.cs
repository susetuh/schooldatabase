﻿using StudentsDB.Entities;

namespace StudentsRepository.Interfaces
{
    public interface IStudentRepository : IRepository<Student>
    {
        void ChangeName(int id, string dto);
    }
}
