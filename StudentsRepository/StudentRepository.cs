﻿using StudentsRepository.Interfaces;
using StudentsDB;
using StudentsDB.Entities;
using System.Collections.Generic;
using System.Linq;

namespace StudentsRepository
{
    public class StudentRepository : IStudentRepository
    {
        public StudentRepository()
        {
            if (DataBase.students.Count == 0)
            {
                DataBase.students.AddRange(new[] {
                new Student { Name = "Johnny", Age = 21, School = "Harvard", ID = 1 },
                new Student { Name = "Ann", Age = 20, School = "Princeton", ID = 2 },
                new Student { Name = "William", Age = 22, School = "Yale", ID = 3 },
                });
            }
        }

        public Student Get(int id)
        {
            return DataBase.students.FirstOrDefault(x => x.ID == id);
        }

        public List<Student> GetAll()
        {
            return DataBase.students;
        }

        public void Add(Student student)
        {
            student.ID = DataBase.students.Max(x => x.ID) + 1;
            DataBase.students.Add(student);
        }

        public void Update(int id, Student student)
        {
            var studentDB = DataBase.students.FirstOrDefault(x => x.ID == id);
            studentDB.Name = student.Name;
            studentDB.Age = student.Age;
            studentDB.School = student.School;
        }

        public void ChangeName(int id, string name)
        {
            var studentDB = DataBase.students.FirstOrDefault(x => x.ID == id);
            studentDB.Name = name;
        }

        public void Delete(int id)
        {
            DataBase.students.RemoveAll(x => x.ID == id);
        }
    }
}
